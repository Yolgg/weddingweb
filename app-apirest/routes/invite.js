'use strict'

var express = require('express');
var InviteController = require('../controllers/invite');

var router = express.Router();

var multipart = require('connect-multiparty');
/*var multipartMiddleware = multipart({ uploadDir: './uploads' });*/

router.get('/home', InviteController.home);
router.post('/test', InviteController.test);
router.post('/save-invite', InviteController.saveInvite);
router.get('/invite/:id?',InviteController.getInvite);
router.get('/invites', InviteController.getInvites);
router.put('/invite/:id', InviteController.updateInvite);
/*router.post('/upload-image/:id', multipartMiddleware, ProjectController.uploadImage);
router.get('/get-image/:image', ProjectController.getImageFile);*/

module.exports = router;