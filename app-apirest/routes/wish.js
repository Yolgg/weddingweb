'use strict'

var express = require('express');
var WishController = require('../controllers/wish');

var router = express.Router();

var multipart = require('connect-multiparty');
/*var multipartMiddleware = multipart({ uploadDir: './uploads' });*/

router.get('/home', WishController.home);
router.post('/test', WishController.test);
router.get('/wish/:id?',WishController.getWish);
router.get('/wishes', WishController.getWishes);
router.put('/wish/:id', WishController.updateWish);
/*router.post('/upload-image/:id', multipartMiddleware, ProjectController.uploadImage);
router.get('/get-image/:image', ProjectController.getImageFile);*/

module.exports = router;