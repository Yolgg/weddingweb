'use strict'

var Invite = require('../models/invite');
var fs = require('fs');
var path = require('path');

var controller = {
	
	home: function(req, res){
		return res.status(200).send({
			message: 'Soy la home'
		});
	},

	test: function(req, res){
		return res.status(200).send({
			message: "Soy el metodo o accion test del controlador de invites"
		});
	},

	//permite guardar proyecto en bbdd 
	saveInvite: function(req, res){
		var invite = new Invite();
		
		var params = req.body;
		invite.firstname = params.firstname;
		invite.lastname = params.lastname;
		invite.email = params.email;
		invite.intolerance = params.intolerance;
		invite.coming = params.coming;
		

		invite.save((err, inviteStored) => {
			if(err) return res.status(500).send({message: 'Error al guardar.'});

			if(!inviteStored) return res.status(404).send({message: 'No se ha podido guardar la confirmación.'});

			return res.status(200).send({invites: inviteStored});
		});
	},

	getInvite: function(req, res){
		var inviteId = req.params.id;

		if(inviteId == null) return res.status(404).send({message: 'El asistente no existe!.'});

		Invite.findById(inviteId, (err, invite) => {

			if(err) return res.status(500).send({message: 'Error al devolver los datos.'});

			if(!invite) return res.status(404).send({message: 'El asistente no existe.'});

			return res.status(200).send({
				invite
			});

		});
	},

	getInvites: function(req, res){

		Invite.find({}).exec((err, invites) => {

			if(err) return res.status(500).send({message: 'Error al devolver los datos.'});

			if(!invites) return res.status(404).send({message: 'No hay usuarios que mostrar.'});

			return res.status(200).send({invites});
		});

	},
	updateInvite: function(req, res){
		var inviteId = req.params.id;
		var update = req.body;

		Project.findByIdAndUpdate(inviteId, update, {new:true}, (err, projectUpdated) => {
			if(err) return res.status(500).send({message: 'Error al actualizar'});

			if(!inviteUpdated) return res.status(404).send({message: 'No existe el invitado para actualizar'});

			return res.status(200).send({
				invite: inviteUpdated
			});
		});

	},

	deleteInvite: function(req, res){
		var inviteId = req.params.id;

		Invite.findByIdAndRemove(invitetId, (err, inviteRemoved) => {
			if(err) return res.status(500).send({message: 'No se ha podido borrar el proyecto'});

			if(!inviteRemoved) return res.status(404).send({message: "No se puede eliminar ese proyecto."});

			return res.status(200).send({
				invite: inviteRemoved
			});
		});
	},

};

	module.exports = controller;
