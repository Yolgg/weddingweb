'use strict'

var Wish = require('../models/wish');
var fs = require('fs');
var path = require('path');

var controller = {
	
	home: function(req, res){
		return res.status(200).send({
			message: 'Soy la home wish'
		});
	},

	test: function(req, res){
		return res.status(200).send({
			message: "Soy el metodo o accion test del controlador de wishes"
		});
	},

	//permite mostrar deseo de bbdd

	getWish: function(req, res){
		var wishId = req.params.id;

		if(inviteId == null) return res.status(404).send({message: 'El deseo no existe!.'});

		Wish.findById(inviteId, (err, wish) => {

			if(err) return res.status(500).send({message: 'Error al devolver los datos.'});

			if(!wish) return res.status(404).send({message: 'El asistente no existe.'});

			return res.status(200).send({
				wish
			});

		});
	},
	getWishes: function(req, res){

		Wish.find({}).exec((err, wishes) => {

			if(err) return res.status(500).send({message: 'Error al devolver los datos.'});

			if(!wishes) return res.status(404).send({message: 'No hay deseos que mostrar.'});

			return res.status(200).send({wishes});
		});

	},

	updateWish: function(req, res){
		var wishId = req.params.id;
		var update = req.body;

		Wish.findByIdAndUpdate(wishId, update, {new:true}, (err, wishUpdated) => {
			if(err) return res.status(500).send({message: 'Error al actualizar'});

			if(!wishUpdated) return res.status(404).send({message: 'No existe el invitado para actualizar'});

			return res.status(200).send({
				wish: wishUpdated
			});
		});

	},

	