'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InviteSchema = Schema({

	firstname:String,
	lastname: String,
	email : String,
	intolerance: String,
	coming: Boolean

});

module.exports = mongoose.model('Invite', InviteSchema);
// invites  --> guarda los documents en la coleccion