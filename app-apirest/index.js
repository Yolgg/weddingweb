'use strict'

var mongoose = require('mongoose');
var app =require('./app');
var port = process.env.PORT || 8080;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/wedding')
		.then(() => {
			console.log("Conexión a la bbdd establecida con éxito");

			//Creación del servidor

			app.listen(port, () => {

				console.log("Servidor corriendo correctamente en la url localhost:3700");

			});
		})
		.catch(err => console.log(err));



